﻿using System;

using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowStripControls;
using System.IO;
using TestStack.White.Factory;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Winium.Cruciatus.Core;
using System.Windows.Automation;
using Winium.Cruciatus.Extensions;
using System.Globalization;

namespace notepadProj
{
    [TestClass]
    public class NotepadTest
    {

        String randomFileName;
        String fileMenu;
        String saveAs;
        String save;


        [TestInitialize]
        public void init()
        {

            CultureInfo ci = CultureInfo.CurrentUICulture;

            if (ci.Name.Equals("ru-RU"))
            {
                fileMenu = "Файл";
                saveAs = "Сохранить как...";
                save = "Сохранить как";
            }
            else
            {
                fileMenu = "File";
                saveAs = "Save as...";
                save = "Save as";
            }

        }

        private static string RandomString(int length)
        {
        Random random = new Random();

        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZa0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [TestMethod]
        public void SavingFileTest()
        {
            var notepad = Application.Launch("notepad");
            SearchCriteria searchCriteria = SearchCriteria.ByClassName("Notepad");

            var winMain = notepad.GetWindow(searchCriteria, InitializeOption.NoCache);

            var textArea = winMain.Get<TextBox>(SearchCriteria.ByAutomationId("15"));
            String randomText = RandomString(new Random().Next(300));
            textArea.Text = randomText;

            var menu = winMain.Get<MenuBar>(SearchCriteria.ByAutomationId("MenuBar"));


            var btnSaveAs = menu.MenuItem(fileMenu, saveAs);
            
            btnSaveAs.Click();

            var winSaveFileDialog = winMain.ModalWindow(save);

            var tbFileName = winSaveFileDialog.MdiChild(SearchCriteria.ByAutomationId("1001"));
            randomFileName = "D:\\" + RandomString(10) + ".txt";
            tbFileName.Enter(randomFileName);

            var btnSave = winSaveFileDialog.Get<Button>(SearchCriteria.ByAutomationId("1"));
            btnSave.Click();

            notepad.Close();

            Assert.IsTrue(File.Exists(randomFileName));
            Assert.AreEqual(File.ReadAllText(randomFileName), randomText);
        }

        [TestCleanup]
        public void Dispose()
        {
            if (File.Exists(randomFileName))
            {
                File.Delete(randomFileName);
            }
        }
    }
}
